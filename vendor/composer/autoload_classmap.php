<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'AddAuthors' => $baseDir . '/app/database/migrations/2014_03_27_144409_add_authors.php',
    'Author' => $baseDir . '/app/models/Author.php',
    'AuthorController' => $baseDir . '/app/controllers/AuthorController.php',
    'AuthorsController' => $baseDir . '/app/controllers/AuthorsController.php',
    'BaseController' => $baseDir . '/app/controllers/BaseController.php',
    'CreateAuthorsTable' => $baseDir . '/app/database/migrations/2014_03_27_143731_create_authors_table.php',
    'DatabaseSeeder' => $baseDir . '/app/database/seeds/DatabaseSeeder.php',
    'HomeController' => $baseDir . '/app/controllers/HomeController.php',
    'IlluminateQueueClosure' => $vendorDir . '/laravel/framework/src/Illuminate/Queue/IlluminateQueueClosure.php',
    'SessionHandlerInterface' => $vendorDir . '/symfony/http-foundation/Symfony/Component/HttpFoundation/Resources/stubs/SessionHandlerInterface.php',
    'TestCase' => $baseDir . '/app/tests/TestCase.php',
    'User' => $baseDir . '/app/models/User.php',
);
