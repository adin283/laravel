<?php

use Illuminate\Support\Facades\View;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('users', function(){
	$users = User::all();
	
	return View::make('users')->with('users', $users);
});

Route::get('home', 'HomeController@showWelcome');

Route::get('/authors', 'AuthorsController@index');

Route::get('/author', array('as' => 'author', 'uses' => 'AuthorController@index'));
Route::get('/authordetail/{id}', array('as' => 'authordetail', 'uses' => 'AuthorController@view'));

Route::get('/author/newAuthor', array('as' => 'newAuthor', 'uses' => 'AuthorController@newAuthor'));
Route::post('/author/create', array('before' => 'csrf', 'uses' => 'AuthorController@create'));

Route::get('/authordetail/{id}/edit', array('as' => 'editAuthor', 'uses' => 'AuthorController@edit'));
Route::put('/author/update', array('before' => 'csrf', 'uses' => 'AuthorController@update'));
Route::delete('/author/delete', array('before' => 'csrf', 'uses' => 'AuthorController@delete'));