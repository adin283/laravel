<?php
use Illuminate\Support\Facades\View;
class AuthorsController extends BaseController {
	
	public $restful = true;
	
	public $layout = 'layouts.default';
	
	public function index() {
		$view = View::make('authors.index', array('name'=>'Kevin'))
			->with('age', '27');
		$view->location = 'Hangzhou';
		$view['specilty'] = 'PHP';
		$view->arr = array('key1' => 'value1', 'key2' => 'value2');
		$this->layout->title = 'Kevin Layout';
		$this->layout->content = $view;
	}
}