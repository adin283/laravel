<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddAuthors extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('authors')->insert(array(
			'name' => 'Kevin Hawk',
			'bio' => 'Kevin Hawk is a really greate developer!',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
		));
		
		DB::table('authors')->insert(array(
			'name' => 'Sandra',
			'bio' => 'Sandra is a beautiful girl!',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
		));
		DB::table('authors')->insert(array(
			'name' => 'Mark Twain',
			'bio' => 'Mark Twain was a prominent American author of the 19th century.',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
		));
		DB::table('authors')->insert(array(
			'name' => 'Martin',
			'bio' => 'Martin is a really greate author!',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
		));
		DB::table('authors')->insert(array(
			'name' => 'Jane Doe',
			'bio' => 'Jane Doe is a really greate author!',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
		));
		DB::table('authors')->insert(array(
			'name' => 'Bill Bo',
			'bio' => 'Bill is lorem ipsum dolor sit amet, consectertur adipiscing elit!',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
		));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('authors')->where('name', '=', 'Kevin Hawk')->delete();
		DB::table('authors')->where('name', '=', 'Sandra')->delete();
		DB::table('authors')->where('name', '=', 'Mark Twain')->delete();
		DB::table('authors')->where('name', '=', 'Martin')->delete();
		DB::table('authors')->where('name', '=', 'Jane Doe')->delete();
		DB::table('authors')->where('name', '=', 'Bill Bo')->delete();
	}

}
