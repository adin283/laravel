@extends('layouts.default2')

@section('content')
	<h1>Kevin Homepage</h1>
	
	<ul>
	@foreach ($authors as $author)
	<li>{{ HTML::linkRoute('authordetail', $author->name, array($author->id)) }}</li>
	@endforeach
	</ul>
	
	<p>{{ HTML::linkRoute('newAuthor', 'New Author')}}</p>
@stop