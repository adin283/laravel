@extends('layouts.default2')

@section('content')
	<h1>Add New Author</h1>
	
	@include('common.authorErrors')

	{{ Form::open(array('url' => 'author/create', 'method' => 'POST')) }}
	{{ Form::token() }}
	<p>
		{{ Form::label('name', 'Name') }}<br />
		{{ Form::text('name', Input::old('name')) }}<br />
		
		{{ Form::label('bio', 'Biography') }}<br />
		{{ Form::textarea('bio', Input::old('bio')) }}<br/>
	</p>
	
	<p>{{ Form::submit('Add Author')}}</p>
	
	{{ Form::close() }}
@stop