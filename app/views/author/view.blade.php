@extends('layouts.default2')

@section('content')

<h1>{{ HTML::entities($author->name) }}</h1>

<p>{{ e($author->bio) }}</p>

<p><small>{{ $author->updated_at }}</small></p>

<span>
	{{ HTML::linkRoute('author', 'Authors') }} |
	{{ HTML::linkRoute('editAuthor', 'Edit', array($author->id)) }}
	{{ Form::open(array('url' => '/author/delete', 'method' => 'DELETE', 'style' => 'display:inline;')) }}
	{{ Form::token() }}
	{{ Form::hidden('id', $author->id) }}
	{{ Form::submit('Delete') }}
	{{ Form::close() }}
</span>
@stop